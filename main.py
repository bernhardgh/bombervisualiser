import pygame
import argparse
import os.path
import time
import roundJson
import threading

BLACK = (0, 0, 0)
DARK_GRAY = (20, 20, 20)
GREY = (60, 60, 60)
LIGHT_RED = (200, 50, 50)

player_color = {'A': (51, 51, 102),
                'B': (51, 102, 51),
                'C': (102, 51, 51),
                'D': (51, 24, 51)}


class ImagesLibrary:
    def __init__(self, width, height):
        self.block_size = (width, height)
        self.half_block = (width / 2, height / 2)
        self.images = {}

    def add_image(self, key, filename):
        self.images[key] = pygame.image.load(filename).convert_alpha()
        self.images[key] = pygame.transform.scale(self.images[key], self.block_size)

    def get_image(self, key):
        return self.get_images([key])[0]

    def get_images(self, keys):
        images = []
        for key in keys:
            images.append(self.images.get(key))
        return images

    def block_position(self, x, y):
        return x * self.block_size[0], y * self.block_size[1]


class GamePathHelper:
    def __init__(self, game_directory=''):
        self.game_directory = game_directory
        self.current_round = 0

    @property
    def current_round_path(self):
        return self.game_directory + '/' + str(self.current_round) + '/state.json'

    def next_round(self, num_rounds=1):
        for next_round_count in range(num_rounds):
            self.current_round += 1
            if not os.path.isfile(self.current_round_path):
                self.current_round -= 1
                return False
        return True

    def previous_round(self, num_rounds=1):
        self.current_round = max(0, self.current_round - num_rounds) if self.current_round > 0 else 0

    def reset(self):
        self.current_round = 0

    def go_to_latest_round(self):
        while self.next_round():
            pass

    def get_previous_round_moves(self, player_key, count=5):
        if self.current_round > 0:
            old_round = self.current_round - 1
            oldest_round = max(0, self.current_round - count)
            while old_round >= oldest_round:
                move_path = os.path.join(self.game_directory, str(old_round), player_key, 'move.txt')
                if os.path.isfile(move_path):
                    with open(move_path, 'r') as f:
                        value = f.readline().strip()
                        yield int(value) if int(value) != -1 else 0
                else:
                    yield -1
                old_round -= 1


def window_header(live, current_round, seed=None):
    return 'Bomberman!!  ' + ('LIVE!!' if live else 'Offline') + (' (' + str(seed) + ') ' if seed else '') + ' - ' + str(current_round)


def update_info(player_info, canvas, size, image_library, path_helper):
    player_area_size = (size[0], size[1] / len(player_info))

    big_font = pygame.font.Font(None, 120)
    medium_font = pygame.font.Font(None, 48)

    player_canvas = pygame.Surface(player_area_size)

    # sort player by points
    # players = sorted(list(player_info.values()), key=lambda p: p.points, reverse=True)

    grey_bomb = image_library.get_image('-B')
    explosion = image_library.get_image('*')

    for value, player_key in enumerate(sorted(player_info.keys())):
        player = player_info[player_key]
        player_canvas.fill(GREY)
        pygame.draw.rect(player_canvas, player_color[player.key], player_canvas.get_rect(), 50)
        pygame.draw.rect(player_canvas, DARK_GRAY, player_canvas.get_rect(), 15)

        text = big_font.render(player.key, 1, DARK_GRAY)
        key_height = text.get_rect().height
        player_canvas.blit(text, (30, 30))

        text = medium_font.render(player.name, 1, DARK_GRAY)
        player_canvas.blit(text, (100, 30))
        if player.dead:
            dead_cross_out_rectangle = text.get_rect()
            dead_cross_out_rectangle.left = 100
            dead_cross_out_rectangle.top = 30 + dead_cross_out_rectangle.height / 2
            dead_cross_out_rectangle.height = 5
            pygame.draw.rect(player_canvas, LIGHT_RED, dead_cross_out_rectangle, 0)

        text = medium_font.render(str(player.points), 1, DARK_GRAY)
        player_canvas.blit(text, (30, 30 + key_height))

        image = image_library.get_image(player.key + 'B')
        for index in range(player.bag):
            if index < player.available_bombs:
                player_canvas.blit(image, (100 + image_library.block_size[0] * index, 80))
            else:
                player_canvas.blit(grey_bomb, (100 + image_library.block_size[0] * index, 80))

        history = ['.', '^', '<', '>', 'v', '-B', 'T']
        for index, move in enumerate(path_helper.get_previous_round_moves(player_key, 7)):
            image = image_library.get_image(history[move if move <= 6 else 0] if move >= 0 else 'X')
            player_canvas.blit(image, (100 + image_library.block_size[0] * index, 130))

        text = medium_font.render(' x ' + str(player.radius), 1, DARK_GRAY)
        right_offset = size[0] - 30 - image_library.block_size[0] - text.get_rect().width
        player_canvas.blit(explosion, (right_offset, 30))
        player_canvas.blit(text, (right_offset + image_library.block_size[0], 30))

        canvas.blit(player_canvas, (0, value * player_area_size[1]))


def init_image_library(grid_block_width, grid_block_height):
    # Build up library of images to be used
    image_library = ImagesLibrary(grid_block_width, grid_block_height)
    image_library.add_image('A', 'resources/player_blue.png')
    image_library.add_image('C', 'resources/player_red.png')
    image_library.add_image('B', 'resources/player_green.png')
    image_library.add_image('D', 'resources/player_purple.png')
    image_library.add_image('@', 'resources/on_bomb.png')
    image_library.add_image('AB', 'resources/bomb_blue.png')
    image_library.add_image('CB', 'resources/bomb_red.png')
    image_library.add_image('BB', 'resources/bomb_green.png')
    image_library.add_image('DB', 'resources/bomb_purple.png')
    image_library.add_image('-B', 'resources/bomb_grey.png')
    image_library.add_image('*', 'resources/explosion.png')
    image_library.add_image('$', 'resources/super_powerup.png')
    image_library.add_image('!', 'resources/radius_powerup.png')
    image_library.add_image('&', 'resources/bag_powerup.png')
    image_library.add_image('#', 'resources/iwall.png')
    image_library.add_image('+', 'resources/wall.png')
    image_library.add_image(' ', 'resources/ground.png')
    image_library.add_image('0', 'resources/0.png')
    image_library.add_image('1', 'resources/1.png')
    image_library.add_image('2', 'resources/2.png')
    image_library.add_image('3', 'resources/3.png')
    image_library.add_image('4', 'resources/4.png')
    image_library.add_image('5', 'resources/5.png')
    image_library.add_image('6', 'resources/6.png')
    image_library.add_image('7', 'resources/7.png')
    image_library.add_image('8', 'resources/8.png')
    image_library.add_image('9', 'resources/9.png')
    image_library.add_image('^', 'resources/arrow_up.png')
    image_library.add_image('v', 'resources/arrow_down.png')
    image_library.add_image('<', 'resources/arrow_left.png')
    image_library.add_image('>', 'resources/arrow_right.png')
    image_library.add_image('.', 'resources/nothing.png')
    image_library.add_image('T', 'resources/trigger.png')
    image_library.add_image('X', 'resources/empty.png')
    return image_library


def main():
    # Get directory of game to play
    parser = argparse.ArgumentParser()
    parser.add_argument('directory',
                        help='The directory where the game is stored.')
    parser.add_argument('-l', '--live', action='store_true', dest='live',
                        help='Indicates if the game is live, meaning that the highest '
                             'round number in the game directory will be displayed.')
    parser.add_argument('--no-info', action='store_true', dest='no_info',
                        help='Flag to hide information.')
    parser.print_help()
    args = parser.parse_args()

    print("Live game!!" if args.live else "Offline game.")
    # Bool that indicates if this is a live game or offline game
    live_game = args.live

    print("-== Controls ==-\n")
    print("Q: quit")
    if not live_game:
        print("R: Reset Game")
        print("Right: Next Round")
        print("Left: Previous Round")
        print("Up: 10 Rounds forward")
        print("Down: 10 Rounds back")
        print("S: Start/Stop auto-run")
    print("\n-==============-\n")

    if not os.path.isdir(args.directory):
        print("The specified game directory is not valid!!")
        return
    elif not live_game and not os.path.isdir(os.path.join(args.directory, '0')):
        print("The specified game directory does not have round information")
        return

    # Print out received directory
    game_dir = args.directory
    print("Directory: " + str(game_dir))

    show_info = not args.no_info

    # Create path helper with directory received as argument
    path_helper = GamePathHelper(game_dir)

    if live_game:
        path_helper.go_to_latest_round()

    # Initialise pygame
    pygame.init()
    pygame.display.set_caption(window_header(live_game, path_helper.current_round))

    # Set up window and canvas
    game_area_size = (800, 800)
    # hacky way to not show info... decrease the canvas width to 0
    info_area_size = (400 if show_info else 0, game_area_size[1])
    window_size = (game_area_size[0] + info_area_size[0], game_area_size[1])
    game_window = pygame.display.set_mode(window_size)

    # Display some text
    info_canvas = pygame.Surface(info_area_size)
    info_canvas.fill(GREY)

    game_canvas = pygame.Surface(game_area_size)
    game_canvas.fill(GREY)

    game_window.blit(game_canvas, (0, 0))
    game_window.blit(info_canvas, (game_area_size[0], 0))
    pygame.display.flip()

    # wait until a file is available
    while not os.path.exists(path_helper.current_round_path):
        for event in pygame.event.get():
            if event.type is pygame.QUIT:
                quit()
    # give the game engine some time to finish off with the first rounds json file
    # NOTE: not very robust, but from searching there doesn't seems to be a proper
    # way to determine if a file is still being used.
    if live_game:
        time.sleep(0.5)

    game_round = roundJson.RoundJson(path_helper.current_round_path)

    image_library = init_image_library(game_area_size[0] / game_round.width, game_area_size[1] / game_round.height)

    update_info(game_round.player_info, info_canvas, info_area_size, image_library, path_helper)
    game_window.blit(info_canvas, (game_area_size[0], 0))
    pygame.display.flip()

    last_round = -1
    last_round_timer = time.time()
    
    main.running = False
    def run():
        if main.running:
            if path_helper.next_round():
                threading.Timer(0.4, run).start()
            else:
                main.running = False

    while True:
        # Check for events
        for event in pygame.event.get():
            if event.type is pygame.QUIT:
                main.running = False
                return
            elif event.type is pygame.KEYDOWN:
                keys = pygame.key.get_pressed()
                if keys[pygame.K_q]:
                    main.running = False
                    return
                elif not live_game:
                    if keys[pygame.K_LEFT]:
                        path_helper.previous_round()
                    elif keys[pygame.K_RIGHT]:
                        path_helper.next_round()
                    elif keys[pygame.K_DOWN]:
                        path_helper.previous_round(10)
                    elif keys[pygame.K_UP]:
                        path_helper.next_round(10)
                    elif keys[pygame.K_r]:
                        path_helper.reset()
                    elif keys[pygame.K_s]:
                        if main.running:
                            main.running = False
                        else:
                            main.running = True
                            run()

        if live_game:
            if time.time() - last_round_timer > 5:
                return
            path_helper.go_to_latest_round()

        if path_helper.current_round != last_round:
            last_round_timer = time.time()
            last_round = path_helper.current_round

            pygame.display.set_caption(window_header(live_game, path_helper.current_round, game_round.seed))

            if os.path.exists(path_helper.current_round_path):
                # if the round was updated read the new json file
                game_round = roundJson.RoundJson(path_helper.current_round_path)

                # Reset canvas
                game_canvas.fill(GREY)

                # Loop through all the positions and draw the appropriate image
                for x in range(game_round.width):
                    for y in range(game_round.height):
                        entity_keys = game_round.get_object_keys_at(x, y)
                        images = image_library.get_images(entity_keys)

                        for image in images:
                            if not image:
                                print(entity_keys)
                            # Draw other image if available
                            game_canvas.blit(image, image_library.block_position(x, y))

                # Update the viewport
                game_window.blit(game_canvas, (0, 0))
                update_info(game_round.player_info, info_canvas, info_area_size, image_library, path_helper)
                game_window.blit(info_canvas, (game_area_size[0], 0))
                pygame.display.flip()
        
        pygame.time.wait(50)

if __name__ == '__main__':
    """
    Entry point of application
    """
    main()
